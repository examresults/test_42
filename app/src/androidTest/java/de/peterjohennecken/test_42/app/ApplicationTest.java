/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package de.peterjohennecken.test_42.app;

import android.app.Application;
import android.test.ApplicationTestCase;
import de.peterjohennecken.test_42.httputil.HttpHelper;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }


    public void test42() throws Exception {
        HttpHelper helper = new HttpHelper();
        assertEquals(42, helper.get42());
    }

    public void testHtml() throws Exception {
        HttpHelper helper = new HttpHelper();
        assertTrue(!helper.getHtml().isEmpty());
    }
}