/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package de.peterjohennecken.test_42.httputil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpHelper {
    private URL url;

    public String getHtml() {
        try {
            url = new URL("http://peter-johennecken.de");
        } catch (MalformedURLException e) {
            return e.getLocalizedMessage();
        }
        HttpURLConnection con;
        String ret = "";
        try {
            con = (HttpURLConnection) url.openConnection();
            //con.setRequestMethod("GET");
            con.setUseCaches(false);
            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) ret = "false";
            else {
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = reader.readLine()) != null) {
                    ret += inputLine;
                }
                reader.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public int get42() {
        return 42;
    }
}
